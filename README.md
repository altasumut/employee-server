[![Build Status](https://app.travis-ci.com/altasumut/employee-server.svg?branch=master)](https://app.travis-ci.com/altasumut/employee-server)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=altasumut_employee-server&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=altasumut_employee-server)

## Execution

```sh
docker-compose up -d
```

## Swagger

Please, reach to the API contract in the following link:

```sh
http://localhost:8080/swagger-ui.html
```

or you can use postman collection under assets folder:

```sh
assets/postman_collection.json
```

## Transactions

| Scenarios | Status |
| ------ | ------ |
| Added -> InCheck | OK |
| Added -> InCheck -> SecurityApprove | OK |
| Added -> InCheck -> SecurityApprove -> WorkPermitApprove -> Approved -> Active | OK |
| Added -> InCheck -> WorkPermitApprove | OK |
| Added -> InCheck -> WorkPermitApprove -> SecurityApprove -> Approved -> Active | OK |