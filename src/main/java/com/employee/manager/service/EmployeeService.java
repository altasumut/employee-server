package com.employee.manager.service;

import com.employee.manager.entity.Address;
import com.employee.manager.entity.Employee;
import com.employee.manager.entity.Validation;
import com.employee.manager.model.dto.EmployeeDto;
import com.employee.manager.model.enums.ApproveType;
import com.employee.manager.model.enums.EmployeeEvents;
import com.employee.manager.model.enums.EmployeeStates;
import com.employee.manager.repo.AddressRepository;
import com.employee.manager.repo.EmployeeRepository;
import com.employee.manager.repo.ValidationRepository;
import com.employee.manager.util.MessageConstant;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;


@Slf4j
@Service
public class EmployeeService {


    @Autowired
    private final EmployeeRepository employeeRepository;

    @Autowired
    private final AddressRepository addressRepository;

    @Autowired
    private final ValidationRepository validationRepository;
    @Autowired
    private final StateMachineFactory<EmployeeStates, EmployeeEvents> factory;

    @Autowired
    private ModelMapper modelMapper;


    public EmployeeService(EmployeeRepository employeeRepository, AddressRepository addressRepository, ValidationRepository validationRepository, StateMachineFactory<EmployeeStates, EmployeeEvents> machineFactory) {

        this.employeeRepository = employeeRepository;
        this.addressRepository = addressRepository;
        this.validationRepository = validationRepository;
        this.factory = machineFactory;
    }

    public Employee createEmployee(EmployeeDto employeeDto) {


        try {
            Employee newEmployee = modelMapper.map(employeeDto, Employee.class);

            Address address = newEmployee.getAddress();
            Validation validation = new Validation();

            if (address == null) {
                log.info("we are unable to acquire the address info for the employee.");
                return null;
            }

            this.validationRepository.save(validation);
            this.addressRepository.save(address);

            newEmployee.setValidation(validation);
            newEmployee.setEmployeeState(EmployeeStates.ADDED);
            newEmployee.setCreationOn(new Date());

            return this.employeeRepository.save(newEmployee);

        } catch (Exception ex) {
            log.error("Error occurred while creating a new employee. Error ::" + ex.getMessage());
        }

        return null;
    }

    public Employee changeToInCheckState(Long employeeId, String inCheckStateChangeMessage) {

        Pair<Employee, StateMachine<EmployeeStates, EmployeeEvents>> stateMachinePair = this.build(employeeId);

        Employee employee = stateMachinePair.getFirst();

        if (employee.getEmployeeState() != EmployeeStates.ADDED) {

            log.info("The employee state is not ADDED. We can only change to the employee state IN_CHECK from the ADDED!");

            return employee;
        }

        StateMachine<EmployeeStates, EmployeeEvents> sm = stateMachinePair.getSecond();

        Message<EmployeeEvents> inCheckMessage = MessageBuilder.withPayload(EmployeeEvents.CHECK).setHeader(MessageConstant.EMPLOYEE_ID_HEADER, employeeId).setHeader("IN_CHECK_CHANGE_MESSAGE", inCheckStateChangeMessage).build();
        sm.sendEvent(inCheckMessage);

        return employee;
    }

    public Employee changeToApproveState(Long employeeId, ApproveType approveType, String inCheckStateChangeMessage) {

        Pair<Employee, StateMachine<EmployeeStates, EmployeeEvents>> stateMachinePair = this.build(employeeId);

        Employee employee = stateMachinePair.getFirst();

        if (employee.getEmployeeState() != EmployeeStates.IN_CHECK) {

            log.info("The employee state is not ADDED. We can only change to the employee state APPROVE from the IN_CHECK!");

            return employee;
        }

        employee.getValidation().setApprove(approveType);
        employeeRepository.save(employee);

        if (employee.getValidation().getApprove()) {
            StateMachine<EmployeeStates, EmployeeEvents> sm = stateMachinePair.getSecond();

            Message<EmployeeEvents> inCheckMessage = MessageBuilder.withPayload(EmployeeEvents.APPROVE).setHeader(MessageConstant.EMPLOYEE_ID_HEADER, employeeId).setHeader("IN_CHECK_CHANGE_MESSAGE", inCheckStateChangeMessage).build();
            sm.sendEvent(inCheckMessage);
        }

        return employee;
    }

    public Pair<Employee, StateMachine<EmployeeStates, EmployeeEvents>> build(Long employeeId) {

        Employee employee = this.employeeRepository.findById(employeeId).orElse(null);
        String employeeIdKey = Long.toString(Objects.requireNonNull(employee).getId());

        StateMachine<EmployeeStates, EmployeeEvents> stateMachine = this.factory.getStateMachine(employeeIdKey);
        stateMachine.stop();

        stateMachine.getStateMachineAccessor()
                .doWithAllRegions(sma -> {

                    sma.addStateMachineInterceptor(new StateMachineInterceptorAdapter<>() {

                        @Override
                        public void preStateChange(

                                State<EmployeeStates, EmployeeEvents> states,
                                Message<EmployeeEvents> messages, Transition<EmployeeStates, EmployeeEvents> transitions,
                                StateMachine<EmployeeStates, EmployeeEvents> stateMachine1,
                                StateMachine<EmployeeStates, EmployeeEvents> rootStateMachine

                        ) {
                            Optional.ofNullable(messages).flatMap(

                                    msg -> Optional.ofNullable(
                                            (Long) msg.getHeaders().getOrDefault(MessageConstant.EMPLOYEE_ID_HEADER, -1L))).ifPresent(

                                    employeeId1 -> {

                                        Employee emp = employeeRepository.findById(employeeId1).orElse(null);
                                        Objects.requireNonNull(emp).setEmployeeState(states.getId());

                                        employeeRepository.save(emp);
                                    });

                        }
                    });

                    EmployeeStates employeeState = employee.getEmployeeState();

                    DefaultStateMachineContext<EmployeeStates, EmployeeEvents> smc = new DefaultStateMachineContext<>(employeeState,
                            null,
                            null,
                            null);

                    sma.resetStateMachine(smc);
                });

        stateMachine.start();

        return Pair.of(employee, stateMachine);
    }
}
