package com.employee.manager.repo;

import com.employee.manager.entity.Validation;
import org.springframework.data.repository.CrudRepository;

public interface ValidationRepository extends CrudRepository<Validation, Long> {

}
