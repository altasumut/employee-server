package com.employee.manager.repo;

import com.employee.manager.entity.Address;
import org.springframework.data.repository.CrudRepository;


public interface AddressRepository extends CrudRepository<Address, Long> {

}
