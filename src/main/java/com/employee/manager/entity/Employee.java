package com.employee.manager.entity;

import com.employee.manager.model.enums.EmployeeStates;
import com.employee.manager.validator.ValidEmail;
import com.employee.manager.validator.ValidPhoneNumber;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity(name = "employee")
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_on", updatable = false)
    private Date creationOn;

    @NotBlank
    @Column(name = "first_name")
    private String firstName;

    @NotBlank
    @Column(name = "last_name")
    private String lastName;

    @ValidEmail(message = "provide a valid email address")
    @Column(name = "email")
    private String email;

    @ValidPhoneNumber(message = "provide a valid phone number")
    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Positive
    @Column(name = "age")
    private Integer age;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private EmployeeStates employeeState;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "validation_id", referencedColumnName = "id")
    private Validation validation;
}