package com.employee.manager.entity;

import com.employee.manager.model.enums.ApproveType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Entity(name = "validation")
@Table(name = "validation")
public class Validation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private UUID uuid;

    @Column(name = "security")
    private Boolean security = false;

    @Column(name = "work_permit")
    private Boolean workPermit = false;

    @Transient
    @JsonIgnore
    @OneToOne(mappedBy = "validation")
    private Employee employee;

    public boolean getApprove() {
        return (this.security && this.workPermit);
    }

    public void setApprove(ApproveType type) {
        if (type == ApproveType.SECURITY) this.security = true;
        else this.workPermit = true;
    }
}
