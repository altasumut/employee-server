package com.employee.manager.util;

import com.employee.manager.entity.Employee;
import com.employee.manager.model.enums.EmployeeEvents;
import com.employee.manager.model.enums.EmployeeStates;
import com.employee.manager.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ApproveGuard implements Guard<EmployeeStates, EmployeeEvents> {

    private final EmployeeRepository employeeRepository;

    @Override
    public boolean evaluate(StateContext<EmployeeStates, EmployeeEvents> stateContext) {
        Long employeeId = (Long) stateContext.getMessage().getHeaders().getOrDefault(MessageConstant.EMPLOYEE_ID_HEADER, -1L);
        Employee employee = this.employeeRepository.findById(employeeId).orElseThrow(NullPointerException::new);
        return employee.getValidation().getApprove();
    }
}