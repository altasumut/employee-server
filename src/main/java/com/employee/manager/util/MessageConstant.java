package com.employee.manager.util;


public class MessageConstant {


    public static final String INTERNAL_SERVER_ERROR_MSG = "Internal server error ";
    public static final String EMPLOYEE_ID_HEADER = "employeeId";
    public static final String EMPLOYEE_NOT_CREATE_MSG = "Employee not created!!";
    public static final String EMPLOYEE_STATE_NOT_UPDATED_MSG = "Employee state doesn't change to the IN_CHECK!!";

    private MessageConstant() {

        throw new UnsupportedOperationException();
    }

}
