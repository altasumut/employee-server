package com.employee.manager.util;

import org.springframework.http.HttpStatus;

import java.util.LinkedHashMap;
import java.util.Map;


public class ApiResponseMessage {

    static final String TAG_STATUS = "status";
    static final String TAG_IS_SUCCESSFUL = "isSuccessful";
    static final String TAG_MESSAGE = "message";
    static Map<String, Object> apiResponse;

    private ApiResponseMessage() {
        throw new IllegalStateException(ApiResponseMessage.class.getName());
    }

    public static Map<String, Object> getGenericApiResponse(Boolean isSuccessful, HttpStatus httpStatusCode, String message) {

        apiResponse = new LinkedHashMap<>();

        apiResponse.put(TAG_IS_SUCCESSFUL, isSuccessful);
        apiResponse.put(TAG_STATUS, httpStatusCode.value());
        apiResponse.put(TAG_MESSAGE, message);

        return apiResponse;
    }


    public static Map<String, Object> getInternalServerError() {

        apiResponse = new LinkedHashMap<>();

        apiResponse.put(TAG_IS_SUCCESSFUL, Boolean.FALSE);
        apiResponse.put(TAG_STATUS, HttpStatus.INTERNAL_SERVER_ERROR.value());
        apiResponse.put(TAG_MESSAGE, "Internal server error. please contact support !!");

        return apiResponse;
    }

}
