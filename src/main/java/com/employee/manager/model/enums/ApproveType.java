package com.employee.manager.model.enums;

public enum ApproveType {
    SECURITY,
    WORK_PERMIT
}
