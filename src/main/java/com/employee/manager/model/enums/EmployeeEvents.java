package com.employee.manager.model.enums;


public enum EmployeeEvents {

    CHECK,

    APPROVE,

    ACTIVE
}
