package com.employee.manager.model.enums;


public enum EmployeeStates {

    ADDED,

    IN_CHECK,

    APPROVED,

    ACTIVE
}