package com.employee.manager.model.dto;

import lombok.Data;


@Data
public class EmployeeDto {

    private String firstName;

    private String lastName;

    private String email;

    private String phoneNumber;

    private Integer age;

    private AddressDto addressDto;

    private ValidationDto validationDto;
}
