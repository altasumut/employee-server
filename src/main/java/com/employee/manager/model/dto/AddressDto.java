package com.employee.manager.model.dto;

import lombok.Data;


@Data
public class AddressDto {


    private String street;

    private String state;

    private String country;

    private int zip;
}
