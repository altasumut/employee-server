package com.employee.manager.model.dto;

import lombok.Data;


@Data
public class ValidationDto {

    private boolean security = false;

    private boolean workPermit = false;
}
