package com.employee.manager.api;

import com.employee.manager.entity.Employee;
import com.employee.manager.model.dto.EmployeeDto;
import com.employee.manager.model.enums.ApproveType;
import com.employee.manager.service.EmployeeService;
import com.employee.manager.util.ApiResponseMessage;
import com.employee.manager.util.MessageConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Slf4j
@Validated
@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Operation(summary = "create an employee in the recruiting platform")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Create employee using the dto", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Employee.class))}),
            @ApiResponse(responseCode = "422", description = MessageConstant.EMPLOYEE_NOT_CREATE_MSG, content = @Content),
            @ApiResponse(responseCode = "500", description = MessageConstant.INTERNAL_SERVER_ERROR_MSG, content = @Content)})

    @PostMapping(value = "/create")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody EmployeeDto employeeDto) {

        try {
            Employee employee = employeeService.createEmployee(employeeDto);

            if (employee != null) {

                return new ResponseEntity<>(employee, new HttpHeaders(), HttpStatus.CREATED);
            }

            return new ResponseEntity<>(ApiResponseMessage.getGenericApiResponse(Boolean.FALSE, HttpStatus.UNPROCESSABLE_ENTITY,
                    MessageConstant.EMPLOYEE_NOT_CREATE_MSG), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception ex) {

            log.error(MessageConstant.INTERNAL_SERVER_ERROR_MSG + ex.getMessage());
            return new ResponseEntity<>(ApiResponseMessage.getInternalServerError(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
     * This endpoint to change the state of a given employee to "In-CHECK" or any of the states defined above in the state machine
     * */
    @Operation(description = "employee onboarding progress: change state from ADDED to IN_CHECK")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "change employee state to the IN_CHECK using employee Id", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Employee.class))}),
            @ApiResponse(responseCode = "422", description = MessageConstant.EMPLOYEE_STATE_NOT_UPDATED_MSG, content = @Content),
            @ApiResponse(responseCode = "500", description = MessageConstant.INTERNAL_SERVER_ERROR_MSG, content = @Content)})

    @PutMapping(value = "/incheck/{id}")
    public ResponseEntity<Object> changeStateInCheck(@PathVariable(value = "id") long id) {

        try {
            Employee employee = employeeService.changeToInCheckState(id, "changing the employee state to the IN_CHECK");

            if (employee != null) {
                return new ResponseEntity<>(employee, new HttpHeaders(), HttpStatus.OK);
            }

            return new ResponseEntity<>(ApiResponseMessage.getGenericApiResponse(Boolean.FALSE, HttpStatus.UNPROCESSABLE_ENTITY,
                    MessageConstant.EMPLOYEE_STATE_NOT_UPDATED_MSG), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception ex) {

            log.error(MessageConstant.INTERNAL_SERVER_ERROR_MSG + ex.getMessage());
            return new ResponseEntity<>(ApiResponseMessage.getInternalServerError(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping(value = "/approve/{id}/{approveType}")
    public ResponseEntity<Object> changeStateApprove(@PathVariable(value = "id") long id, @PathVariable(value = "approveType") ApproveType approveType) {

        try {
            Employee employee = employeeService.changeToApproveState(id, approveType, "changing the employee state to the APPROVE");

            if (employee != null) {
                return new ResponseEntity<>(employee, new HttpHeaders(), HttpStatus.OK);
            }

            return new ResponseEntity<>(ApiResponseMessage.getGenericApiResponse(Boolean.FALSE, HttpStatus.UNPROCESSABLE_ENTITY,
                    MessageConstant.EMPLOYEE_STATE_NOT_UPDATED_MSG), new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception ex) {

            log.error(MessageConstant.INTERNAL_SERVER_ERROR_MSG + ex.getMessage());
            return new ResponseEntity<>(ApiResponseMessage.getInternalServerError(), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}