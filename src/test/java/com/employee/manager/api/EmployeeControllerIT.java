package com.employee.manager.api;

import com.employee.manager.entity.Employee;
import com.employee.manager.model.dto.AddressDto;
import com.employee.manager.model.dto.EmployeeDto;
import com.employee.manager.model.dto.ValidationDto;
import com.employee.manager.model.enums.EmployeeStates;
import com.employee.manager.repo.AddressRepository;
import com.employee.manager.repo.EmployeeRepository;
import com.employee.manager.repo.ValidationRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerIT {


    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ValidationRepository validationRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Test
    void givenEmployeeDto_CreateEmployeeWithAddedState() throws Exception {

        EmployeeDto employeeDto = createEmployeeDto();

        mockMvc.perform(post("/api/employee/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(employeeDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is("Lorem")))
                .andExpect(jsonPath("$.lastName", is("Impus")))
                .andExpect(jsonPath("$.age", is(27)))
                .andExpect(jsonPath("$.email", is("lorem.impus@temp-mail.com")))
                .andExpect(jsonPath("$.phoneNumber", is("123-456-7890")))
                .andExpect(jsonPath("$.employeeState", is("ADDED")));

    }

    @Test
    void givenEmployeeDto_Put() throws Exception {

        EmployeeDto employeeDto = createEmployeeDto();

        Employee employee = modelMapper.map(employeeDto, Employee.class);
        employee.setCreationOn(new Date());
        employee.setEmployeeState(EmployeeStates.ADDED);

        addressRepository.save(employee.getAddress());
        validationRepository.save(employee.getValidation());
        employeeRepository.save(employee);


        mockMvc.perform(put("/api/employee/incheck/" + 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("Lorem")))
                .andExpect(jsonPath("$.lastName", is("Impus")))
                .andExpect(jsonPath("$.age", is(27)))
                .andExpect(jsonPath("$.email", is("lorem.impus@temp-mail.com")))
                .andExpect(jsonPath("$.phoneNumber", is("123-456-7890")))
                .andExpect(jsonPath("$.employeeState", is("IN_CHECK")));

        mockMvc.perform(put("/api/employee/approve/" + 1L + "/SECURITY")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employeeState", is("IN_CHECK")));

        mockMvc.perform(put("/api/employee/approve/" + 1L + "/WORK_PERMIT")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employeeState", is("ACTIVE"))); // <-- Active state happened automatically because It meets all the conditions.
    }

    private EmployeeDto createEmployeeDto() {

        EmployeeDto employeeDto = new EmployeeDto();


        employeeDto.setFirstName("Lorem");
        employeeDto.setLastName("Impus");
        employeeDto.setEmail("lorem.impus@temp-mail.com");
        employeeDto.setPhoneNumber("123-456-7890");
        employeeDto.setAge(27);


        AddressDto addressDto = new AddressDto();

        addressDto.setStreet("Some dummy address");
        addressDto.setState("Istanbul");
        addressDto.setCountry("Turkey");
        addressDto.setZip(12345);

        ValidationDto validationDto = new ValidationDto();

        validationDto.setSecurity(false);
        validationDto.setWorkPermit(false);

        employeeDto.setAddressDto(addressDto);
        employeeDto.setValidationDto(validationDto);

        return employeeDto;
    }


}